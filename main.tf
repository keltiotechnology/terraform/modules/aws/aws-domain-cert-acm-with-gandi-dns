resource "aws_acm_certificate" "cert" {
  domain_name               = var.domain_name
  subject_alternative_names = var.subject_alternative_names
  validation_method         = "DNS"

  lifecycle {
    create_before_destroy = true
  }
}

resource "gandi_livedns_record" "gandi_acm_validation" {
  for_each = {
    for dvo in aws_acm_certificate.cert.domain_validation_options : dvo.domain_name => {
      name   = trim(dvo.resource_record_name, ".")
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }

  zone   = join(".", slice(split(".", each.value.name), length(split(".", each.value.name)) - 2, length(split(".", each.value.name))))
  name   = join(".", slice(split(".", each.value.name), 0, length(split(".", each.value.name)) - 2))
  type   = each.value.type
  ttl    = 360
  values = [each.value.record]
}

resource "aws_acm_certificate_validation" "ovh_certificate_validation" {
  certificate_arn         = aws_acm_certificate.cert.arn
  validation_record_fqdns = [for record in gandi_livedns_record.gandi_acm_validation : "${record.name}.${record.zone}"]
}
