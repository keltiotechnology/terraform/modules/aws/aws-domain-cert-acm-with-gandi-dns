variable "domain_name" {
  type        = string
  description = "The domain name alias to the Application Load balancer"
}

variable "subject_alternative_names" {
  type        = list(string)
  description = "DNS name of AWS Load balancer"
  default     = []
}
