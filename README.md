# Aws Domain cert on Certificate Manager with Gandi DNS

This module :
- register the domain name in AWS Certificate manager and validate it. Amazon will generate a free SSL certificate for our website.

## Usage
```hcl-terraform
provider "aws" {}
provider "gandi" {}

module "aws_domain_cert_acm_with_gandi_dns" {
  source                    = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-domain-cert-acm-with-gandi-dns"

  domain_name               = "<Domain name registered in OVH>"
  subject_alternative_names = ["Some alternative name"]
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 2.0 |
| <a name="requirement_gandi"></a> [gandi](#requirement\_gandi) | 2.0.0-rc3 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.58.0 |
| <a name="provider_gandi"></a> [gandi](#provider\_gandi) | 2.0.0-rc3 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_acm_certificate.cert](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate) | resource |
| [aws_acm_certificate_validation.ovh_certificate_validation](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/acm_certificate_validation) | resource |
| [gandi_livedns_record.gandy_acm_validation](https://registry.terraform.io/providers/psychopenguin/gandi/2.0.0-rc3/docs/resources/livedns_record) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_domain_name"></a> [domain\_name](#input\_domain\_name) | The domain name alias to the Application Load balancer | `string` | n/a | yes |
| <a name="input_subject_alternative_names"></a> [subject\_alternative\_names](#input\_subject\_alternative\_names) | DNS name of AWS Load balancer | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_acm_certificate_arn"></a> [aws\_acm\_certificate\_arn](#output\_aws\_acm\_certificate\_arn) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
