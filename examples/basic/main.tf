provider "aws" {}
provider "gandi" {}

module "aws_certificates" {
  source = "../../"

  domain_name               = "example.keltio.fr"
  subject_alternative_names = ["www.example.keltio.fr"]
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 2.0"
    }
    gandi = {
      source  = "psychopenguin/gandi"
      version = "2.0.0-rc3"
    }
  }
}
